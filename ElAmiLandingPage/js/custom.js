$(document).ready(function () {
    var total;
    var lesson180=null;
    var lesson1 = null;
    var lesson2 = null;
    var lesson3 = null;
    var lesson4 = null;
    var lesson5 = null;
    var meal = null;
    var rade = null;
    var coupon=null;

    $("#lesson_1").change(function () {
        lesson1 = $(this).val().match(/\d+/);
        CalculatingAmount();
    });

    $("#lesson_2").change(function () {
        lesson2 = $(this).val().match(/\d+/);
        CalculatingAmount();
    });

    $("#lesson_3").change(function () {
        lesson3 = $(this).val().match(/\d+/);
        CalculatingAmount();
    });

    $("#lesson_4").change(function () {
        lesson4 = $(this).val().match(/\d+/);
        CalculatingAmount();
    });

    $("#lesson_5").change(function () {
        lesson5 = $(this).val().match(/\d+/);
        CalculatingAmount();
    });

    $("#lesson").change(function () {
        if ($(this).is(':checked')) {
            lesson180 = 180;
        }
        else {
            lesson180 = 0;
        }
        CalculatingAmount();
    });


    $("#meal").change(function () {
        meal = $(this).val().match(/\d+/);
        if (meal != null) {
            $("#mealCount").val(1);
        }
        else {
            $("#mealCount").val("");
        }
        CalculatingAmount();
    });

    $("#rade").change(function () {
        rade = $(this).val().match(/\d+/);
        if (rade != null) {
            $("#radeCount").val(1);
        }
        else {
            $("#radeCount").val("");
        }
        CalculatingAmount();
    });

    $("#mealCount,#radeCount").change(function () {
        CalculatingAmount();
    });

    function CalculatingAmount() {
        total = 0;
        (lesson1 != null) ? total += parseInt(lesson1) : total += 0;
        (lesson2 != null) ? total += parseInt(lesson2) : total += 0;
        (lesson3 != null) ? total += parseInt(lesson3) : total += 0;
        (lesson4 != null) ? total += parseInt(lesson4) : total += 0;
        (lesson5 != null) ? total += parseInt(lesson5) : total += 0;

        if (parseInt(total) >= 200) {
            total = 150;
        }

        if (coupon != null) {
            if (coupon == 0) {
                total = 0;
            }
            else {
                if (total > 100) {
                    total = parseInt(coupon);
                }
            }
        }

        (meal != null) ? total += (parseInt(meal) * parseInt($("#mealCount").val())) : total += 0;
        (rade != null) ? total += (35 * parseInt($("#radeCount").val())) : total += 0;

        (lesson180 != null) ? total += lesson180 : total += 0;

        $("#total").text(total);
        $("#totalHidden").val(total);
    }

    $("#check_coupon").click(function () {
        var code = $(".coupon_field").val();
        switch (code) {
            case "4321":
                coupon = 0;
                meal = 0;
                $(".awesome_un_success_box").css("display", "none");
                $(".coupon_message").css("display", "none");
                $(".awesome_success_box").css("display", "block");
                break;
            case "1111":
                coupon = 0;
                $(".awesome_un_success_box").css("display", "none");
                $(".coupon_message").css("display", "none");
                $(".awesome_success_box").css("display", "block");
                break;
            case "6789":
                coupon = 100;
                $(".awesome_un_success_box").css("display", "none");
                $(".coupon_message").css("display", "none");
                $(".awesome_success_box").css("display", "block");
                break;
            case "9876":
                coupon = 100;
                $(".awesome_un_success_box").css("display", "none");
                $(".coupon_message").css("display", "none");
                $(".awesome_success_box").css("display", "block");
                break;
            case "1234":
                coupon = 100;
                $(".awesome_un_success_box").css("display", "none");
                $(".coupon_message").css("display", "none");
                $(".awesome_success_box").css("display", "block");
                break;
            case "12345":
                coupon = 100;
                $(".awesome_un_success_box").css("display", "none");
                $(".coupon_message").css("display", "none");
                $(".awesome_success_box").css("display", "block");
                break;
            default:
                coupon = null;
                $(".awesome_success_box").css("display", "none");
                $(".awesome_un_success_box").css("display", "block");
                $(".coupon_message").css("display", "block");
                break;
        }
        CalculatingAmount();
    });
});