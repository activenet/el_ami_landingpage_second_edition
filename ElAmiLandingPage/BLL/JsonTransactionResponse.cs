﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElAmiLandingPage.BLL
{
    public class JsonTransactionResponse
    {
        public string transactionID { get; set; }
        public string ValidationString { get; set; }
        public string URL { get; set; }
    }
}