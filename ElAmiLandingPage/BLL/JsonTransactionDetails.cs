﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElAmiLandingPage.BLL
{
    public class JsonTransactionDetails
    {
        public string terminal { get; set; }
        public string user { get; set; }
        public string password { get; set; }
        public string GoodURL { get; set; }
        public string ErrorUrl { get; set; }
        public string Currency { get; set; }
        public string Total { get; set; }
        public string NotificationGoodMail { get; set; }
        public string NotificationErrorMail { get; set; }
        public string ParamX { get; set; }
    }
}