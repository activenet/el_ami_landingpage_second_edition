﻿using ElAmiLandingPage.BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Configuration;

namespace ElAmiLandingPage.BLL
{
    public class PaymentRequest
    {
        public object JsonTransact { get; set; }
        private string totalPayment { get; set; }
        private string OrderNum { get; set; }

        public PaymentRequest(string total,string order)
        {
            this.totalPayment = total;
            this.OrderNum = order;
            SubmitJsonData();
        }

        protected void SubmitJsonData()
        {
            // An object to contain the response
            var jsonTransactionResponse = new JsonTransactionResponse()
            {
                transactionID = string.Empty,
                ValidationString = string.Empty,
                URL = string.Empty
            };

            // Fill in the JSON parameters (use the corresponding documents to determine the parameters needed for your unique payment page).
            var jsonTransactionRequest = new JsonTransactionDetails()
            {
                terminal = "5774948",
                user = "elami",
                password = "u6YbqFQQ",
                GoodURL = ConfigurationManager.AppSettings["GoodUrl"],
                ErrorUrl = ConfigurationManager.AppSettings["ErrorUrl"],
                Currency = "1",
                Total = this.totalPayment,
                NotificationGoodMail = ConfigurationManager.AppSettings["NotificationGoodMail"],
                NotificationErrorMail = ConfigurationManager.AppSettings["NotificationErrorMail"],
                ParamX = OrderNum
            };

            // Creating the JSON object and encode it to bytes (The encoding choice here is the default for the application/x-www-form-urlencoded contentType).

            var jsonRequestSerializer = new DataContractJsonSerializer(typeof(JsonTransactionDetails));
            var jsonMemoryStream = new MemoryStream();
            jsonRequestSerializer.WriteObject(jsonMemoryStream, jsonTransactionRequest);
            jsonMemoryStream.Position = 0;
            var requestJsonString = (new StreamReader(jsonMemoryStream).ReadToEnd()).Replace(@"\/", "/");
            var jsonRequestBytes = Encoding.UTF8.GetBytes(requestJsonString);
            // Create the request object for the initial request.
            var jsonWebRequest = WebRequest.Create("https://gateway20.pelecard.biz/PaymentGW/Init");
            jsonWebRequest.ContentType = "application/x-www-form-urlencoded";
            jsonWebRequest.ContentLength = jsonRequestBytes.Length;
            jsonWebRequest.Method = "POST";
            // Send the JSON data using the request object.
            using (var jsonRequestStream = jsonWebRequest.GetRequestStream())
            {
                jsonRequestStream.Write(jsonRequestBytes, 0, jsonRequestBytes.Length);
                jsonRequestStream.Close();
            }
            // Read the response JSON and decode it.
            var jsonResponse = jsonWebRequest.GetResponse();

            using (var jsonResponseStream = new StreamReader(jsonResponse.GetResponseStream()))
            {
                var responseJsonString = jsonResponseStream.ReadToEnd();

                var jsonResponseSerializer = new DataContractJsonSerializer(typeof(JsonTransactionResponse));

                jsonMemoryStream = new MemoryStream(Encoding.UTF8.GetBytes(responseJsonString));

                jsonTransactionResponse = (JsonTransactionResponse)jsonResponseSerializer.ReadObject(jsonMemoryStream);

                this.JsonTransact = jsonTransactionResponse.URL;
            }
        }
    }
}