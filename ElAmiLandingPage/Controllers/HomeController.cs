﻿using ElAmiLandingPage.BLL;
using ElAmiLandingPage.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ElAmiLandingPage.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Thanks()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactSubmitForm(LandingModel model)
        {
            EmailMessageModel emailMessgae = new EmailMessageModel(model, Request.Form["total"]);
            if(Request.Form["contribution"]!=null)
            {
                model.Contribution = "כן";
            }
            else
            {
                model.Contribution = "לא";
            }
            if (Request.Form["baby"] != null)
            {
                model.Baby = "כן";
            }
            else
            {
                model.Baby = "לא";
            }
            if (ModelState.IsValid)
            {
                var message = new MailMessage(ConfigurationManager.AppSettings["EmailFrom"], ConfigurationManager.AppSettings["EmailTo"]);
                var client = new SmtpClient();
                message.Subject = "פניה מדף צור קשר";
                message.IsBodyHtml = true;
                message.Body = emailMessgae.GetEmailHtmlMessage();
                client.Send(message);

                TempData["success"] = true;
            }
            if (model.Payment == "טלפון" && (bool)TempData["success"] == true)
            {
                return Redirect("/Home/Thanks");
            }
            if (model.Payment == "כרטיס אשראי" && (bool)TempData["success"] == true)
            {
                Session["price"] = int.Parse(Request.Form["total"]) * 100;
                return Redirect("/Home/ToPay");
            }
            return View("Index");
        }

        public ActionResult ToPay()
        {
            string totalPayment = Session["price"].ToString();
            string orderNum = Session["order"].ToString();
            Session["order"] = null;
            Session["price"] = null;
            PaymentRequest paymentRequest = new PaymentRequest(totalPayment,orderNum);
            ViewData["jsonTransactionResponse"] = paymentRequest.JsonTransact;
            return View();
        }

        public ActionResult GoodUrl()
        {
            return Content("<p style='font-size:30px; font-family:Arial; margin-top:50px; direction:rtl;'>התשלום בוצע בהצלחה והפרטים נקלטו ונשלחו בהצלחה.</p>");
        }

        public ActionResult ErrorUrl()
        {
            return Content("<p style='font-size:30px; font-family:Arial; margin-top:50px; direction:rtl;'>התשלום לא בוצע, נא בדוק פרטים שהוזנו</p>");
        }
    }
}
