﻿using ElAmiLandingPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ElAmiLandingPage.Models
{
    public class EmailMessageModel
    {
        private LandingModel Model { get; set; }
        private string Price { get; set; }

        public EmailMessageModel(LandingModel model,string price)
        {
            this.Model = model;
            this.Price = price;
        }

        public string GetEmailHtmlMessage()
        {
            return this.InitEmailHtmlMessage();
        }

        private string InitEmailHtmlMessage()
        {
            string message = "<table dir='rtl' style='text-align:right; margin:0 0 0 auto;'>" +
                              "<thead>" +
                                   "<tr>" +
                                       "<th colspan=2>פרטים אישיים</th>"
                                     + "</tr>" +
                              "</thead>" +
                              "<tbody>" +
                              "<tr>" +
                                      "<th>מס' הזמנה:</th><td>" + this.GenerateOrderNum() + "</td>"
                                     + "</tr>" +
                                    "<tr>" +
                                       "<th>תשלום באמצעות:</th><td>" + Model.Payment + "</td>"
                                     + "</tr>" +
                                    "<tr>" +
                                       "<th>שם:</th><td>" + Model.Name + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                       "<th>שם משפחה:</th><td>" + Model.LastName + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                       "<th>טלפון:</th><td>" + Model.Phone + "</td>" +
                                    "</tr>" +
                                     "<tr>" +
                                       "<th>נייד:</th><td>" + Model.Mobile + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                       "<th>כתובת:</th><td>" + Model.Adress + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                       "<th>עיר:</th><td>" + Model.City + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                       "<th>מיקוד:</th><td>" + Model.Index + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                       "<th>מייל:</th><td>" + Model.Email + "</td>" +
                                    "</tr>" +
                               "</tbody>"
                           + "</table><br/><br/>";

            message += "<table dir='rtl' style='text-align:right; margin:0 0 0 auto;'>" +
                             "<thead>" +
                                  "<tr>" +
                                      "<th colspan=2>יום עיון</th>"
                                    + "</tr>" +
                             "</thead>" +
                             "<tbody>" +
                                   "<tr>" +
                                      "<th>שיעור 09:30 - 11:05:</th><td>" + Model.Lesson_1 + "</td>"
                                    + "</tr>" +
                                   "<tr>" +
                                      "<th>שיעור 11:30 - 12:30:</th><td>" + Model.Lesson_2 + "</td>" +
                                   "</tr>" +
                                   "<tr>" +
                                      "<th>שיעור 13:15 - 14:30:</th><td>" + Model.Lesson_3 + "</td>" +
                                   "</tr>" +
                                   "<tr>" +
                                      "<th>שיעור 14:50 - 16:10:</th><td>" + Model.Lesson_4 + "</td>" +
                                   "</tr>" +
                                    "<tr>" +
                                      "<th>שיעור 16:30 - 18:00:</th><td>" + Model.Lesson_5 + "</td>" +
                                   "</tr>" +
                                   "<tr>" +
                                      "<th>ארוחה:</th><td>" + Model.Meal + "</td>" +
                                   "</tr>" +
                                    "<tr>" +
                                      "<th>מספר ארוחות:</th><td>" + Model.MealCount + "</td>" +
                                   "</tr>" +
                                   "<tr>" +
                                      "<th>הסעה:</th><td>" + Model.Rade + "</td>" +
                                   "</tr>" +
                                    "<tr>" +
                                      "<th>מספר נוסעים:</th><td>" + Model.RadeCount + "</td>" +
                                   "</tr>" +
                                   "<tr>" +
                                      "<th>קוד קופון:</th><td>" + this.Coupon() + "</td>" +
                                   "</tr>" +
                                   "<tr>" +
                                      "<th>מעוניינת להקדיש שיעור בתרומה:</th><td>" + Model.Contribution + "</td>" +
                                   "</tr>" +
                                   "<tr>" +
                                      "<th>מביאה עימי תינוק ושמרטפית:</th><td>" + Model.Baby + "</td>" +
                                   "</tr>" +
                              "</tbody>"
                          + "</table><br/><br/>";

            message += "<p style='font-size:32px; text-align:right;'>סה''כ לתשלום  " + this.Price + "  ש''ח</p>";

            return message;
        }

        private string GenerateOrderNum()
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 7; ++i)
            {
                sb.Append(random.Next(10));
            }
            HttpContext.Current.Session["order"] = sb.ToString();

            return sb.ToString();
        }

        private string Coupon()
        {
            switch (Model.Coupon)
            {
                case "4321":
                    return Model.Coupon;
                case "1234":
                    return Model.Coupon;
                case "12345":
                    return Model.Coupon;
                case "1111":
                    return Model.Coupon;
                case "6789":
                    return Model.Coupon;
                case "9876":
                    return Model.Coupon;
                default:
                    return Model.Coupon + " קוד קופון שהוזן לא קיים";
            }
        }
    }
}