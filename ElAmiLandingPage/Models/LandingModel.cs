﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElAmiLandingPage.Models
{
    public class LandingModel
    {
        [Required(ErrorMessage = "שדה חובה")]
        public string Name { get; set; }

        [Required(ErrorMessage = "שדה חובה")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "שדה חובה")]
        [RegularExpression(@"^0\d([\d]{0,1})([-]{0,1})\d{7}$", ErrorMessage = "מספר טלפון לא תקין")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "שדה חובה")]
        [RegularExpression(@"^0\d([\d]{0,1})([-]{0,1})\d{7}$", ErrorMessage = "מספר טלפון לא תקין")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "שדה חובה")]
        public string Adress { get; set; }

        [Required(ErrorMessage = "שדה חובה")]
        public string City { get; set; }

        //[Required(ErrorMessage = "שדה חובה")]
        //[RegularExpression("^[1-9][0-9]*$", ErrorMessage = "שדה חייב להכיל מספר")]
        public string Index { get; set; }

        [Required(ErrorMessage = "שדה חובה")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "דוא''ל לא תקין")]
        public string Email { get; set; }

        [Required(ErrorMessage = "בחירה חובה")]
        public string Lesson_1 { get; set; }
        [Required(ErrorMessage = "בחירה חובה")]
        public string Lesson_2 { get; set; }
        [Required(ErrorMessage = "בחירה חובה")]
        public string Lesson_3 { get; set; }
        [Required(ErrorMessage = "בחירה חובה")]
        public string Lesson_4 { get; set; }
        [Required(ErrorMessage = "בחירה חובה")]
        public string Lesson_5 { get; set; }

        public string Meal { get; set; }
        public string MealCount { get; set; }
        public string Rade { get; set; }
        public string RadeCount { get; set; }

        public string Payment { get; set; }
        public string Coupon { get; set; }

        public string Contribution { get; set; }
        public string Baby { get; set; }
    }
}